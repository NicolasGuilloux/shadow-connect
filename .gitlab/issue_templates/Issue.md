# Issue

## Description

<!-- Describe what happened -->

## System and configuration

| | |
| -------------------- | ------------------------------- |
| OS                   | Ubuntu 20.04, MacOS, Windows 10 |
| Branch               | prod, preprod, testing          |
| VM start process     | Working, Not working            |
| Stream start process | Working, Not working            |

## Screenshots

<!-- Please give some screenshots or even a video -->

## Additional context

<!-- Do you have something to add? -->


