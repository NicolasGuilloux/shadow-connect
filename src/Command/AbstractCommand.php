<?php declare(strict_types=1);

namespace ShadowConnect\Command;

abstract class AbstractCommand
{
    /** @var string */
    protected static $defaultName = '';

    public static function getDefaultName(): string
    {
        return static::$defaultName;
    }

    abstract public function execute(): int;
}
