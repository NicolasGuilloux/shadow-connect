<?php declare(strict_types=1);

namespace ShadowConnect\Command;

use ShadowConnect\Api\GapApi;
use ShadowConnect\Facade\Service;

final class StopVmCommand extends AbstractCommand
{
    /** @var string */
    protected static $defaultName = 'stop_vm';

    public function execute(): int
    {
        $gapApi = Service::get(GapApi::class);
        $gapApi->stopVm();

        return 0;
    }
}
