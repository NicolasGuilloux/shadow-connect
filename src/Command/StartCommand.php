<?php declare(strict_types=1);

namespace ShadowConnect\Command;

use ShadowConnect\Api\GapApi;
use ShadowConnect\Executor\RendererExecutor;
use ShadowConnect\Facade\Service;

/**
 * Class StartCommand
 *
 * @package    ShadowConnect\Command
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class StartCommand extends AbstractCommand
{
    /** @var string */
    protected static $defaultName = 'start';

    public function execute(): int
    {
        $gapApi = Service::get(GapApi::class);
        $gapApi->startVm();

        $rendererExecutor = Service::get(RendererExecutor::class);
        $rendererExecutor->start();

        return 0;
    }
}
