<?php declare(strict_types=1);

namespace ShadowConnect\Command;

use ShadowConnect\Facade\Configuration;
use ShadowConnect\Helper\Output;
use ShadowConnect\Kernel;

/**
 * Class ConfigurationWizardCommand
 *
 * @package    ShadowConnect\Command
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class ConfigurationWizardCommand extends AbstractCommand
{
    /** @var string */
    protected static $defaultName = 'config-wizard';

    public function execute(): int
    {
        Output::print("\n");
        Output::print('Welcome to the configuration wizard.', null, Output::BG_COLOR_CYAN);
        Output::print("\n");
        Output::print('Please follow the instruction and let us guide you to configure the Shadow Connect.');
        Output::print("\n\n");

        $templatePath = Kernel::getProjectDir() . '/src/Resources/config.php.template';
        $content = file_get_contents($templatePath);
        $configs = [
            'USERNAME'           => Output::prompt('Email'),
            'RENDERER_EXEC_PATH' => Output::prompt('Path to the renderer (not the launcher!)'),
            'BRANCH'             => Output::prompt('Branch (prod, preprod, testing)', 'prod'),
            'BITRATE'            => Output::prompt('Bitrate (from 5 to 70)', '20'),
            'HEVC'               => Output::prompt('Enable HEVC', 'false'),
            'USB'                => Output::prompt('Enable USB', 'false'),
            'TCP'                => Output::prompt('Enable TCP', 'false'),
            'AUDIO_PCM'          => Output::prompt('Enable high quality audio', 'false'),
            'FULLSCREEN'         => Output::prompt('Enable fullscreen', 'false'),
            'QUICKMENU'          => Output::prompt('Enable Quickmenu', 'false'),
            'MAX_FRAMERATE'      => Output::prompt('Max framerate', 'null'),
            'AUTO_SHUTDOWN'      => Output::prompt('Enable auto shutdown', 'false'),
        ];

        foreach ($configs as $key => $value) {
            $content = str_replace(
                '%' . $key . '%',
                self::parseValue($value),
                $content
            );
        }

        $target = sprintf('%s/config/config.%s.php', Kernel::getProjectDir(), Configuration::get('APP_ENV'));
        file_put_contents($target, $content);

        return 0;
    }

    private static function parseValue(?string $value): string
    {
        if ($value === null || $value === 'null') {
            return 'null';
        }

        $boolValue = filter_var($value, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);

        if ($boolValue !== null) {
            return $boolValue ? 'true' : 'false';
        }

        $intValue = filter_var($value, FILTER_VALIDATE_INT, FILTER_NULL_ON_FAILURE);

        if ($intValue !== null) {
            return (string) $intValue;
        }

        return '\'' . $value . '\'';
    }
}
