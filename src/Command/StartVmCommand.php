<?php declare(strict_types=1);

namespace ShadowConnect\Command;

use ShadowConnect\Api\GapApi;
use ShadowConnect\Facade\Service;

final class StartVmCommand extends AbstractCommand
{
    /** @var string */
    protected static $defaultName = 'start_vm';

    public function execute(): int
    {
        $gapApi = Service::get(GapApi::class);
        $gapApi->startVm();

        return 0;
    }
}
