<?php declare(strict_types=1);

namespace ShadowConnect\Generator;

use ShadowConnect\Api\GapApi;
use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Service;

final class RendererFlagsGenerator
{
    public function __invoke(?string $secondary = null): string
    {
        /** @var GapApi $gapApi */
        $gapApi = Service::get(GapApi::class);
        $vmStatus = $gapApi->getVmStatus();

        if ($vmStatus->isFailed()) {
            $gapApi->startVm();
            $vmStatus = $gapApi->getVmStatus();
        }

        $gapToken = $gapApi->getToken();

        $flags = [
            'ip'      => $vmStatus->getContent()['ip'] ?? null,
            'port'    => $vmStatus->getContent()['port'] ?? null,
            'agent'   => '"' . Configuration::get('RENDERER_AGENT') . '"',
            'bitrate' => Configuration::get('BITRATE') * 1000000,
        ];

        if (!Configuration::get('USB')) {
            $flags['no-usb'] = null;
        }

        if ($secondary !== null || !Configuration::get('QUICKMENU')) {
            $flags['use-cef'] = 'false';
        }

        if (Configuration::get('DISPLAY_SAFE')) {
            $flags['display-safe-mode'] = null;
        }

        if (Configuration::get('HEVC')) {
            $flags['hevc'] = null;
        }

        if (Configuration::get('EXPERIMENTAL_FEATURES')) {
            $flags['experimental'] = null;
        }

        if (Configuration::get('AUDIO_PCM')) {
            $flags['audio-codec'] = 'pcm';
        }

        if (Configuration::get('TCP')) {
            $flags['use-cursor-tcp'] = null;
            $flags['use-audio-tcp'] = null;
            $flags['use-input-tcp'] = null;
            $flags['use-video-tcp'] = null;
        }

        if ($secondary !== null) {
            $flags['secondary-token'] = $gapToken;
            $flags['output-id'] = 1;
            $flags['token-permissions'] = 'controlchan,video,cursor,input';
        } else {
            $flags['token'] = $gapToken;
        }

        $flatFlags = array_map(
            static function ($value, $key) {
                return $value === null
                    ? '--' . $key
                    : '--' . $key . ' ' . $value;
            },
            $flags,
            array_keys($flags)
        );

        return implode(' ', $flatFlags);
    }
}
