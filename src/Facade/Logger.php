<?php declare(strict_types=1);

namespace ShadowConnect\Facade;

final class Logger
{
    public const LEVEL_PRIORITIES = ['debug', 'info', 'warn', 'error'];

    public static function error(string $message, ?string $component = null): void
    {
        self::log('error', $message, $component);
    }

    public static function warn(string $message, ?string $component = null): void
    {
        self::log('warn', $message, $component);
    }

    public static function info(string $message, ?string $component = null): void
    {
        self::log('info', $message, $component);
    }

    public static function debug(string $message, ?string $component = null): void
    {
        self::log('debug', $message, $component);
    }

    private static function log(string $type, string $message, ?string $component = null): void
    {
        $minLevel = array_search(Configuration::get('LOG_LEVEL'), self::LEVEL_PRIORITIES, true);
        $level = array_search($type, self::LEVEL_PRIORITIES, true);

        if ($minLevel > $level) {
            return;
        }

        $now = new \DateTime('now');
        $log = sprintf('[%s][%s]', $now->format('Y-m-d H:i:s'), strtoupper($type));

        if ($component !== null) {
            $log .= sprintf('[%s]', $component);
        }

        $log .= ' ' . $message . PHP_EOL;

        echo $log;
    }
}
