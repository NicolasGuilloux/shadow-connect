<?php declare(strict_types=1);

namespace ShadowConnect\Facade;

use ShadowConnect\Kernel;
use ShadowConnect\Resolver\ConfigurationResolver;

final class Configuration
{
    /** @var array<string|int, mixed>|null */
    private static $config;

    /** @var ConfigurationResolver */
    private static $configurationResolver;

    public static function init(): void
    {
        self::$configurationResolver = Service::get(ConfigurationResolver::class);
        self::$config = [];

        foreach (self::getConfigurationPaths() as $configPath) {
            if (!file_exists($configPath)) {
                continue;
            }

            $import = require $configPath;

            if (is_array($import)) {
                self::$config = array_merge(self::$config, $import);
            }
        }
    }

    /**
     * @return mixed|int|bool|string
     */
    public static function get(string $propertyName, bool $required = true)
    {
        $value = self::$config[$propertyName] ?? null;

        if ($value === null) {
            return (self::$configurationResolver)($propertyName, $required);
        }

        return is_string($value) ? self::resolveParameters($value) : $value;
    }

    /**
     * @return string[]
     */
    private static function getConfigurationPaths(): array
    {
        return [
            Kernel::getProjectDir() . '/config/config.php',
            Kernel::getProjectDir() . '/config/config.' . self::get('APP_ENV') . '.php',
            \getenv('HOME') . '/.config/shadow-connect/config.php',
        ];
    }

    private static function resolveParameters(string $config): string
    {
        preg_match_all('/%(\w*)%/', $config, $matches);

        foreach ($matches[1] as $key) {
            $value = (string) self::get($key);
            /** @var string $config */
            $config = str_replace('%' . $key . '%', $value, $config);
        }

        return $config;
    }
}
