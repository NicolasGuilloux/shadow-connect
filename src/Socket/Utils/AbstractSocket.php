<?php declare(strict_types=1);

namespace ShadowConnect\Socket\Utils;

use ShadowConnect\Api\GapApi;
use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Logger;
use ShadowConnect\Facade\Service;

abstract class AbstractSocket
{
    /** @var int */
    protected static $contentLength = 1024;

    /** @var int */
    protected static $socketTimeout = 600;

    /** @var int */
    protected static $portOffset = 0;

    /** @var resource|null */
    protected $socketClient;

    /** @var GapApi */
    protected $gapApi;

    /** @var string */
    protected $url;

    /** @var string */
    protected $ip;

    /** @var int */
    protected $port;

    public function __construct()
    {
        /** @var GapApi $gapApi */
        $gapApi = Service::get(GapApi::class);
        $this->gapApi = $gapApi;
    }

    protected function parseVmInfo(): void
    {
        /** @var GapApi $gapApi */
        $gapApi = Service::get(GapApi::class);
        $vmStatus = $gapApi->getVmStatus()->getContent();

        $this->ip = $vmStatus['ip'] ?? '';
        $this->port = ($vmStatus['port'] ?? 0) + static::$portOffset;
        $this->url = sprintf('ssl://%s:%d', $this->ip, $this->port);
    }

    /**
     * @return resource
     */
    protected function getSocketClient()
    {
        if ($this->socketClient !== null) {
            return $this->socketClient;
        }

        if ($this->url === null) {
            $this->parseVmInfo();
        }

        $context = stream_context_create([
            'ssl' => [
                'verify_peer'      => false,
                'verify_peer_name' => false,
            ],
        ]);

        $socketClient = stream_socket_client(
            $this->url,
            $errno,
            $errstr,
            static::$socketTimeout,
            STREAM_CLIENT_CONNECT,
            $context
        );

        if (is_bool($socketClient)) {
            $error = sprintf('Failed to connect to the socket: %s (%s)', $errstr, $errno);
            throw new \LogicException($error);
        }

        $this->socketClient = $socketClient;

        return $this->socketClient;
    }

    protected function close(): void
    {
        fclose($this->getSocketClient());
    }

    /**
     * @param array<string, int|string|mixed> $data
     */
    protected function send(array $data): void
    {
        $data['token'] = $this->gapApi->getToken();
        $data['agent'] = Configuration::get('RENDERER_AGENT');
        /** @var string $encodedData */
        $encodedData = json_encode($data);

        fwrite(
            $this->getSocketClient(),
            $encodedData
        );

        Logger::debug('Send to the socket the following data: ' . json_encode($data));
    }

    protected function read(): void
    {
        $content = fread(
            $this->getSocketClient(),
            static::$contentLength
        );

        if ($content !== false && $this->handle($content)) {
            $this->read();
        }
    }

    protected function handle(string $message): bool
    {
        // Override this function to handle the messages from the Socket
        return false;
    }
}
