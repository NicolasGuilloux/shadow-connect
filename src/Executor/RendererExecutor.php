<?php declare(strict_types=1);

namespace ShadowConnect\Executor;

use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Logger;
use ShadowConnect\Facade\Service;
use ShadowConnect\Generator\RendererFlagsGenerator;

class RendererExecutor
{
    /** @var RendererFlagsGenerator */
    protected $flagsGenerator;

    public function __construct()
    {
        /** @var RendererFlagsGenerator $flagsGenerator */
        $flagsGenerator = Service::get(RendererFlagsGenerator::class);
        $this->flagsGenerator = $flagsGenerator;
    }

    public function start(): void
    {
        $commands = $this->getCommands();

        Logger::info('Starting the renderer...');
        Logger::debug('Executing the commands: ' . implode(', ', $commands));
        $this->execute($commands);

        Logger::info('Renderer closed.');
    }

    /**
     * @return string[]
     */
    private function getCommands(): array
    {
        $path = Configuration::get('RENDERER_EXEC_PATH', false);

        if ($path !== null) {
            return [
                sprintf('cd %s', $path),
                sprintf('./%s %s > /dev/null 2>&1', Configuration::get('RENDERER_EXEC_BIN'), ($this->flagsGenerator)()),
            ];
        }

        return [
            sprintf('%s %s > /dev/null 2>&1', Configuration::get('RENDERER_EXEC_BIN'), ($this->flagsGenerator)()),
        ];
    }

    /**
     * @param array|string[]|string $commands
     */
    private function execute($commands): void
    {
        $command = implode(' && ', (array) $commands);
        exec($command);
    }
}
