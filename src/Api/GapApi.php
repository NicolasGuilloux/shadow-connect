<?php declare(strict_types=1);

namespace ShadowConnect\Api;

use ShadowConnect\Api\Utils\AbstractApi;
use ShadowConnect\Api\Utils\JsonResponse;
use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Logger;
use ShadowConnect\Facade\Service;

final class GapApi extends AbstractApi
{
    public const MAXIMUM_RETRY = 5;

    /** @var string */
    private $token;

    /** @var int */
    private $retryCount = 0;

    public function __construct()
    {
        $this->defaultAgent = Configuration::get('RENDERER_AGENT');
        $this->defaultUuid = Configuration::get('UUID');
    }

    public function getBaseUrl(): ?string
    {
        if ($this->baseUrl === null) {
            $this->init();
        }

        return $this->baseUrl;
    }

    public function getToken(): ?string
    {
        if ($this->token === null) {
            $this->init();
        }

        return $this->token;
    }

    public function startVm(): void
    {
        $token = $this->getToken();
        $this->retryCount === 0
            ? Logger::info('Starting the VM...')
            : Logger::debug($this->retryCount + 1 . ' attempt');

        $response = $this->request(
            'POST',
            '/shadow/vm/start',
            ['token' => $token]
        );

        if ($response->isFailed()) {
            throw new \LogicException('Failed to start VM.');
        }

        $statusResponse = $this->getVmStatus();

        if ($statusResponse->isFailed()) {
            if ($this->retryCount > self::MAXIMUM_RETRY) {
                $this->retryCount = 0;
                throw new \LogicException('Failed to start VM.');
            }

            sleep(4);
            $this->retryCount++;
            $this->startVm();
            return;
        }

        $this->retryCount = 0;
        Logger::info('VM started successfully.');
    }

    public function stopVm(): void
    {
        $token = $this->getToken();
        $this->retryCount === 0
            ? Logger::info('Stopping the VM...')
            : Logger::debug($this->retryCount + 1 . ' attempt');

        $response = $this->request(
            'POST',
            '/shadow/vm/stop',
            ['token' => $token]
        );

        if ($response->isFailed()) {
            throw new \LogicException('Failed to stop VM.');
        }

        Logger::info('VM stopped successfully.');
    }

    public function getVmStatus(): JsonResponse
    {
        return $this->request(
            'GET',
            '/shadow/vm/ip',
            ['token' => $this->getToken()]
        );
    }

    public function checkUuid(): void
    {
        $this->request(
            'GET',
            '/shadow/auth_uuid',
            ['token' => $this->getToken()]
        );
    }

    public function registerUuid(string $code): JsonResponse
    {
        return $this->request(
            'GET',
            '/shadow/client/approval',
            [
                'code'  => $code,
                'token' => $this->getToken(),
            ],
            Configuration::get('RENDERER_AGENT'),
            Configuration::get('UUID')
        );
    }

    protected function init(): void
    {
        /** @var TinagApi $tinagApi */
        $tinagApi = Service::get(TinagApi::class);
        $result = $tinagApi->authenticate();

        $this->baseUrl = $result['url'];
        $this->token = $result['token'];
    }
}
