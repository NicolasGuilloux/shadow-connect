<?php declare(strict_types=1);

namespace ShadowConnect\Api;

use ShadowConnect\Api\Utils\AbstractApi;
use ShadowConnect\Facade\Configuration;
use ShadowConnect\Facade\Logger;

final class TinagApi extends AbstractApi
{
    /** @var string */
    protected static $defaultBaseUrl = 'https://tinag.shadow.tech';

    public function __construct()
    {
        $this->defaultAgent = Configuration::get('RENDERER_AGENT');
    }

    /**
     * @return array<string, string>
     */
    public function authenticate(): array
    {
        $username = Configuration::get('USERNAME');
        $password = Configuration::get('PASSWORD');

        Logger::info('Logging in...');
        Logger::debug('The following UUID will be used: ' . Configuration::get('UUID'));
        $url = $this->getBaseUrl($username);

        $response = $this->request(
            'POST',
            $url . '/shadow/auth_login',
            sprintf('email=%s&pwd64=%s', urlencode($username), base64_encode($password)),
            null,
            Configuration::get('UUID')
        );

        $result = $response->getContent();

        if ($result === null || !array_key_exists('token', $result)) {
            throw new \LogicException('Authentication failure');
        }

        Logger::info('Successfully logged in.');

        return [
            'url'   => $url,
            'token' => $result['token'],
        ];
    }

    /**
     * Retrieve GAP URL based on email and targeted service on Shadow's TINAG servers.
     */
    protected function getBaseUrl(string $username): string
    {
        $data = ['email' => $username, 'fmt' => 'json'];
        $response = $this->request('GET', '/gap', $data);
        $result = $response->getContent();

        if ($result === null || !array_key_exists('uri', $result)) {
            throw new \LogicException('No URI found');
        }

        return $result['uri'];
    }
}
