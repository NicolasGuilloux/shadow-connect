<?php declare(strict_types=1);

namespace ShadowConnect\Api\Utils;

use ShadowConnect\Facade\Service;
use ShadowConnect\Handler\ApiErrorHandler;

abstract class AbstractApi
{
    /** @var string */
    protected static $defaultBaseUrl = '';

    /** @var string|null */
    protected $baseUrl;

    /** @var string|null */
    protected $defaultAgent;

    /** @var string|null */
    protected $defaultUuid;

    /**
     * @param array|mixed[]|string|null $data
     */
    protected function request(
        string $method,
        string $url,
        $data = null,
        ?string $userAgent = null,
        ?string $uuid = null
    ): JsonResponse {
        $client = Service::get(CurlClient::class);
        $client->init();

        $userAgent = $userAgent ?? $this->defaultAgent;
        $uuid = $uuid ?? $this->defaultUuid;
        $data = is_array($data) ? http_build_query($data) : $data;
        $url = strpos($url, 'http') === 0
            ? $url
            : ($this->baseUrl ?? static::$defaultBaseUrl) . $url;

        switch ($method) {
            case 'POST':
                $client->setOption(CURLOPT_POST, 1);

                if ($data) {
                    $client->setOption(CURLOPT_POSTFIELDS, $data);
                }

                break;

            case 'PUT':
                $client->setOption(CURLOPT_PUT, 1);
                break;

            default:
                if ($data) {
                    $url .= '?' . $data;
                }
        }

        $client->setOption(CURLOPT_URL, $url);

        if ($userAgent !== null) {
            $client->setOption(CURLOPT_USERAGENT, $userAgent);

            if ($uuid !== null) {
                $client->setOption(CURLOPT_HTTPHEADER, [
                    'X-Shadow-Uuid: ' . $uuid,
                    'X-Shadow-Agent: ' . $userAgent,
                ]);
            }
        }

        $response = $client->execute();

        if ($response->isFailed()) {
            /** @var ApiErrorHandler $handler */
            $handler = Service::get(ApiErrorHandler::class);
            $retry = $handler->handle($response);

            if ($retry) {
                $this->request($method, $url, $data, $userAgent, $uuid);
            }
        }

        return $response;
    }
}
