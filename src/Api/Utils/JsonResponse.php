<?php declare(strict_types=1);

namespace ShadowConnect\Api\Utils;

final class JsonResponse
{
    /**
     * @var int
     */
    private $statusCode;

    /**
     * @var string|null
     */
    private $content;

    public function __construct(int $statusCode, ?string $content)
    {
        $this->statusCode = $statusCode;
        $this->content = $content;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function isFailed(): bool
    {
        return $this->statusCode < 200 || $this->statusCode >= 300;
    }

    public function getRawContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return array|mixed[]|null
     */
    public function getContent(): ?array
    {
        return $this->content !== null ? json_decode($this->content, true) : null;
    }
}
