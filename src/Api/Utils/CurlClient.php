<?php declare(strict_types=1);

namespace ShadowConnect\Api\Utils;

use ShadowConnect\Facade\Logger;

/**
 * Class CurlClient
 *
 * @package    ShadowConnect\Api\Utils
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class CurlClient
{
    /** @var \CurlHandle|resource */
    protected $curl;

    public function init(): self
    {
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);

        return $this;
    }

    public function setOption(int $key, $value): self
    {
        curl_setopt($this->curl, $key, $value);

        return $this;
    }

    public function execute(): JsonResponse
    {
        $result = curl_exec($this->curl);
        $httpcode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $url = curl_getinfo($this->curl, CURLOPT_URL);
        curl_close($this->curl);

        Logger::debug(
            sprintf("URL: %s \tHttpCode: %s \t%s", $url, $httpcode, $result),
            'ApiCall'
        );

        $content = is_string($result) ? $result : null;

        return new JsonResponse($httpcode, $content);
    }
}
