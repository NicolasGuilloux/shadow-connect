<?php declare(strict_types=1);

namespace ShadowConnect\Helper;

/**
 * Class Process
 *
 * @package    ShadowConnect\Helper
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class Process
{
    public static function run(string $command, ...$parameters): string
    {
        $args = array_merge([$command], $parameters);
        $realCommand = sprintf(...$args);
        exec($realCommand, $outputLines, $result);
        $output = implode("\n", $outputLines);

        if ($result !== 1) {
            return $output;
        }

        throw new \LogicException(
            sprintf(
                "Something when wrong during the execution of the command %s\n\n%s",
                $realCommand,
                $output
            )
        );
    }
}
