<?php declare(strict_types=1);

namespace ShadowConnect\Helper;

final class Output
{
    /**
     * Ansi cursor movement
     */
    public const ESC = "\x1b[";
    public const GO_UP = "\033[1A";
    public const GO_DOWN = "\033[1B";
    public const GO_LINE_BEGINNING = "\r";
    public const SAVE_POSITION = "\033[s";
    public const RESTORE_POSITION = "\033[u";

    /**
     * Ansi Text Colors
     */
    public const COLOR_UNSET = '0';
    public const COLOR_BLACK = '0;30';
    public const COLOR_DARK_GRAY = '1;30';
    public const COLOR_BLUE = '0;34';
    public const COLOR_LIGHT_BLUE = '1;34';
    public const COLOR_GREEN = '0;32';
    public const COLOR_LIGHT_GREEN = '1;32';
    public const COLOR_CYAN = '0;36';
    public const COLOR_LIGHT_CYAN = '1;36';
    public const COLOR_RED = '0;31';
    public const COLOR_LIGHT_RED = '1;31';
    public const COLOR_PURPLE = '0;35';
    public const COLOR_LIGHT_PURPLE = '1;35';
    public const COLOR_BROWN = '0;33';
    public const COLOR_YELLOW = '1;33';
    public const COLOR_LIGHT_GRAY = '0;37';
    public const COLOR_WHITE = '1;37';

    /**
     * Ansi Background Colors
     */
    public const BG_COLOR_BLACK = '40';
    public const BG_COLOR_RED = '41';
    public const BG_COLOR_GREEN = '42';
    public const BG_COLOR_YELLOW = '43';
    public const BG_COLOR_BLUE = '44';
    public const BG_COLOR_MAGENTA = '45';
    public const BG_COLOR_CYAN = '46';
    public const BG_COLOR_LIGHT_GRAY = '47';

    /**
     * Characters
     */
    public const CHAR_CHECK = '✓';

    private function __construct()
    {
        // Not instiantiable
    }

    public static function format(string $message, ?string $color = null, ?string $backgroundColor = null): string
    {
        $output = self::genAnsiColor($color ?? self::COLOR_UNSET);

        if ($backgroundColor !== null) {
            $output .= self::genAnsiColor($backgroundColor);
        }

        $output .= $message . self::genAnsiColor(self::COLOR_UNSET);

        return $output;
    }

    public static function print(string $message, ?string $color = null, ?string $backgroundColor = null): string
    {
        $output = self::format($message, $color, $backgroundColor);
        echo $output;

        return $output;
    }

    public static function success(string $message): void
    {
        self::print($message, self::COLOR_LIGHT_GREEN);
    }

    public static function danger(string $message): void
    {
        self::print($message, self::COLOR_LIGHT_RED);
    }

    public static function error(string $message): void
    {
        self::print($message, self::COLOR_WHITE, self::BG_COLOR_RED);
    }

    public static function warning(string $message): string
    {
        return self::print($message, self::COLOR_YELLOW);
    }

    public static function info(string $message): void
    {
        self::print($message, self::COLOR_LIGHT_BLUE);
    }

    public static function prompt(string $message, string $default = null): ?string
    {
        if ($default !== null) {
            $message .= ' [' . self::format($default, self::COLOR_YELLOW) . ']';
        }

        self::print($message . ': ');

        /** @var resource $buffer */
        $buffer = fopen('php://stdin', 'rb');
        /** @var string $input */
        $input = trim(fgets($buffer));
        $parsedInput = ($input !== '' && $input !== 'null') ? $input : null;

        if ($default === null && $parsedInput === null) {
            return self::prompt($message, $default);
        }

        $default = $default !== 'null' ? $default : null;

        return $parsedInput ?? $default;
    }

    private static function genAnsiColor(string $ansiColor): string
    {
        return "\033[" . $ansiColor . 'm';
    }
}
