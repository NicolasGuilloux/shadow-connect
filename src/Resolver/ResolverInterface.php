<?php declare(strict_types=1);

namespace ShadowConnect\Resolver;

/**
 * Interface ResolverInterface
 *
 * @package    ShadowConnect\Resolver
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
interface ResolverInterface
{
    public function supports(string $key): bool;
    public function getPriority(): int;

    /**
     * @return string|int|mixed|null
     */
    public function resolve();
}
