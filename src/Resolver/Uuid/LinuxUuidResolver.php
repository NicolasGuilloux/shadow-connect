<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\Uuid;

use ShadowConnect\Facade\Configuration;
use ShadowConnect\Resolver\AbstractResolver;

/**
 * Class LinuxUuidResolver
 *
 * @package    ShadowConnect\Resolver\Uuid
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class LinuxUuidResolver extends AbstractResolver
{
    /** @var string */
    protected static $propertyName = 'UUID';

    public function supports(string $key): bool
    {
        return parent::supports($key) && Configuration::get('PLATFORM') === 'Linux' && $this->getUuid() !== null;
    }

    public function resolve(): string
    {
        return str_rot13($this->getUuid() ?? '');
    }

    private function getUuid(): ?string
    {
        exec('lsblk -nro SERIAL', $result);

        return $result[0] ?? null;
    }
}
