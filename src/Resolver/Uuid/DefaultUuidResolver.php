<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\Uuid;

use ShadowConnect\Resolver\AbstractResolver;
use ShadowConnect\Resolver\ResolverInterface;

/**
 * Class LinuxUuidResolver
 *
 * @package    ShadowConnect\Resolver\Uuid
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class DefaultUuidResolver extends AbstractResolver
{
    /** @var string */
    protected static $propertyName = 'UUID';

    /** @var int */
    protected static $priority = -100;

    public function resolve(): string
    {
        return str_rot13(uniqid('', true));
    }
}
