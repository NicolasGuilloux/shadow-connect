<?php declare(strict_types=1);

namespace ShadowConnect\Resolver;

use ShadowConnect\Facade\Service;
use ShadowConnect\Resolver\AppEnv\AppEnvResolver;
use ShadowConnect\Resolver\Password\PromptPasswordResolver;
use ShadowConnect\Resolver\Password\SecretToolPasswordResolver;
use ShadowConnect\Resolver\Platform\PlaftormResolver;
use ShadowConnect\Resolver\RendererAgent\LinuxRendererAgentResolver;
use ShadowConnect\Resolver\RendererAgent\MacRendererAgentResolver;
use ShadowConnect\Resolver\Username\PromptUsernameResolver;
use ShadowConnect\Resolver\Uuid\DefaultUuidResolver;
use ShadowConnect\Resolver\Uuid\LinuxUuidResolver;
use ShadowConnect\Resolver\Uuid\MacUuidResolver;

/**
 * Class ConfigurationResolver
 *
 * @package    ShadowConnect\Resolver
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class ConfigurationResolver
{
    /** @var string[] */
    private static $resolverClasses = [
        AppEnvResolver::class,
        DefaultUuidResolver::class,
        LinuxRendererAgentResolver::class,
        LinuxUuidResolver::class,
        MacRendererAgentResolver::class,
        MacUuidResolver::class,
        PlaftormResolver::class,
        PromptPasswordResolver::class,
        PromptUsernameResolver::class,
        SecretToolPasswordResolver::class,
    ];

    /** @var ResolverInterface[] */
    private $resolvers;

    /** @var array<string, mixed> */
    private $cachedValues = [];

    public function __construct()
    {
        $this->resolvers = array_map(
            static function (string $resolverClass) {
                return Service::get($resolverClass);
            },
            static::$resolverClasses
        );

        usort(
            $this->resolvers,
            static function (ResolverInterface $left, ResolverInterface $right) {
                if ($left->getPriority() === $right->getPriority()) {
                    return 0;
                }

                return $left->getPriority() < $right->getPriority();
            }
        );
    }

    public function __invoke(string $key, bool $required = true)
    {
        if (array_key_exists($key, $this->cachedValues)) {
            return $this->cachedValues[$key];
        }

        foreach ($this->resolvers as $resolver) {
            $support = $resolver->supports($key);

            if ($support) {
                $value = $resolver->resolve();
                $this->cachedValues[$key] = $value;

                return $value;
            }
        }

        if (!$required) {
            return null;
        }

        throw new \LogicException('No resolver found for ' . $key);
    }
}
