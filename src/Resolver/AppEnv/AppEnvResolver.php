<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\AppEnv;

use ShadowConnect\Resolver\AbstractResolver;

/**
 * Class AppEnvResolver
 *
 * @package    ShadowConnect\Resolver\AppEnv
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class AppEnvResolver extends AbstractResolver
{
    /** @var string */
    protected static $propertyName = 'APP_ENV';

    public function resolve(): string
    {
        return getenv('APP_ENV') ?: 'local';
    }
}
