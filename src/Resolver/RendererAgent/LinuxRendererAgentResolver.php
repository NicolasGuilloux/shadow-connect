<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\RendererAgent;

/**
 * Class LinuxRendererAgentResolver
 *
 * @package    ShadowConnect\Resolver\RendererAgent
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class LinuxRendererAgentResolver extends AbstractRendererAgentResolver
{
    /** @var string */
    protected static $agent = 'Linux;x64;Chrome 78.0.3904.130;3.4.4';

    /** @var string */
    protected static $platform = 'Linux';
}
