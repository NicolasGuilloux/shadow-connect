<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\RendererAgent;

use ShadowConnect\Facade\Configuration;
use ShadowConnect\Resolver\AbstractResolver;

/**
 * Class AbstractRendererAgentResolver
 *
 * @package    ShadowConnect\Resolver\RendererAgent
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
abstract class AbstractRendererAgentResolver extends AbstractResolver
{
    /** @var string|null */
    protected static $agent;

    /** @var string|null */
    protected static $platform;

    /** @var string */
    protected static $propertyName = 'RENDERER_AGENT';

    public function supports(string $key): bool
    {
        return parent::supports($key) && Configuration::get('PLATFORM') === static::$platform;
    }

    public function resolve(): string
    {
        return static::$agent;
    }
}
