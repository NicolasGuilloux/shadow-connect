<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\RendererAgent;

/**
 * Class MacRendererAgentResolver
 *
 * @package    ShadowConnect\Resolver\RendererAgent
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class MacRendererAgentResolver extends AbstractRendererAgentResolver
{
    /** @var string */
    protected static $agent = 'Mac;x64;Chrome 78.0.3904.130;3.3.2';

    /** @var string */
    protected static $platform = 'Darwin';
}
