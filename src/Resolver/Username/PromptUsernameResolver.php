<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\Username;

use ShadowConnect\Helper\Output;
use ShadowConnect\Resolver\AbstractResolver;

/**
 * Class PromptUsernameResolver
 *
 * @package    ShadowConnect\Resolver\Username
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class PromptUsernameResolver extends AbstractResolver
{
    /** @var string */
    protected static $propertyName = 'USERNAME';

    /** @var int */
    protected static $priority = -100;

    public function resolve(): string
    {
        return Output::prompt('Email');
    }
}
