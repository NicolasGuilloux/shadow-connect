<?php declare(strict_types=1);

namespace ShadowConnect\Resolver\Password;

use ShadowConnect\Facade\Configuration;
use ShadowConnect\Helper\Process;
use ShadowConnect\Resolver\AbstractResolver;

/**
 * Class SecretToolPasswordResolver
 *
 * @package    ShadowConnect\Resolver\Password
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
final class SecretToolPasswordResolver extends AbstractResolver
{
    /** @var string */
    protected static $propertyName = 'PASSWORD';

    public function supports(string $key): bool
    {
        if (!parent::supports($key) || Configuration::get('PLATFORM') !== 'Linux') {
            return false;
        }

        try {
            Process::run('secret-tool --help &> /dev/null');

            return true;
        } catch (\Throwable $e) {
            return false;
        }
    }

    public function resolve()
    {
        while ($this->getPassword() === null) {
            $this->askPassword();
        }

        return $this->getPassword();
    }

    private function getPassword(): ?string
    {
        try {
            $result = Process::run('secret-tool lookup shadow %s', Configuration::get('USERNAME'));
        } catch (\LogicException $e) {
            return null;
        }

        return $result !== '' ? $result : null;
    }

    private function askPassword(): void
    {
        echo 'Please enter your password, it will be stored in your keyring.';

        Process::run(
            'secret-tool store --label=\'Password for Shadow for the username %s\' shadow %s',
            Configuration::get('USERNAME'),
            Configuration::get('USERNAME')
        );
    }
}
