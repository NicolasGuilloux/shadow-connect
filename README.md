# Shadow Connect

Shadow Connect lets the user start his Shadow VM and the renderer right from the CLI with no hassle.

**This repository will not be responsible with anything related to the security of your Shadow account. If somebody is competent enough to check what is done by the code and find a security flaw, use the Issue tab to report it, so we can fix it.**

**Moreover, this program is a very early alpha, and the doc will be oriented for experienced user only. There is not guarantee whatsoever that you can run the program, that it will work or that will not break the whole internet.**

![Showcase](./.gitlab/images/showcase.gif)


## Table of content

1. [Installation](#1-installation)
   - [OS Installation](#os-installation)
   - [Install from the repository](#install-from-the-repository)
2. [Hacking](#2-hacking)
3. [Discord servers](#3-discord-servers)
4. [Maintainers](#4-maintainers)
5. [Disclaimer](#5-disclaimer)

## 1. Installation

### OS Installation

- [![Linux](https://img.shields.io/badge/Install%20on-Linux-red)](./config/distribution/linux)
    - [![Nix](https://img.shields.io/badge/Install%20on-Nix-74b2d8)](./config/distribution/nix)
- [![MacOS](https://img.shields.io/badge/Install%20on-MacOS-lightgrey)](./config/distribution/macos)
- [![Windows](https://img.shields.io/badge/Install%20on-Windows-00a2ed)](./config/distribution/windows)

### Install from the repository

**Composer is not required, it is only there for the tests! No need to use it.**

This application uses PHP with the `curl` extension. Make sure both are available and correctly configured. You also need to setup the application. To do so, simply execute the assistant wizard by typing `./bin/shadow-connect config-wizard`

Here is the available entries if you want to edit the configuration file:

| Property              | Type                       | Default  | Description                                                                            |
| ---                   | ---                        | ---      | ---                                                                                    |
| BRANCH                | `prod`,`preprod`,`testing` | `prod`   | The branch you want to use.                                                            |
| USERNAME              | string                     | x        | Your username. If not set, it will be asked in the CLI                                 |
| RENDERER_EXEC_PATH    | string or null             | x        | The path to the renderer. You need to extract the application if you use the AppImage. |
| RENDERER_EXEC_BIN     | string                     | `Shadow` | The name of the renderer binary.                                                       |
| BITRATE               | integer (from 5 to 70)     | 20       | The maximum bitrate used by the stream in Mbits/s.                                     |
| HEVC                  | boolean                    | false    | Enable HEVC/H265 to improve the quality with low hardware.                             |
| USB                   | boolean                    | false    | Enable USB forwarding (not working on Linux).                                          |
| TCP                   | boolean                    | false    | Enable TCP protocol to improve the input fidelity over speed.                          |
| AUDIO_PCM             | boolean                    | false    | Enable the high quality audio.                                                         |
| FULLSCREEN            | boolean                    | false    | Start the renderer in fullscreen.                                                      |
| QUICKMENU             | boolean                    | true     | Enable the QuickMenu. Disable if the streams crashes.                                  |
| MAX_FRAMERATE         | float or null              | null     | Maximum framerate.                                                                     |
| AUTO_SHUTDOWN         | boolean                    | false    | Automatically shutdown your Shadow when you exit the renderer.                         |

## 2. Hacking

Of course, you can tweak whatever you want. To start the test, you will need to have composer and PHP installed on your machine.

 - Install composer dependencies: `composer install`
 - Execute tests: `composer run test`
 - Execute tests with coverage: `composer run test:coverage`
 - Execute tests with coverage with generation of reports: `composer run test:coverage:reports`
 

## 3. Discord servers

  Find us on Shadow Official Discord servers!

- [Discord Shadow FR](https://discordapp.com/invite/shadowtech)
- [Discord Shadow UK](https://discordapp.com/invite/ShadowEN)
- [Discord Shadow DE](https://discord.gg/shadowde)
- [Discord Shadow US](https://shdw.me/USDiscord)

## 4. Maintainers

![Alex^#1629](https://cdn.discordapp.com/avatars/401575828590428161/36d0ac43c2cb3a72d41c51b0c8375f65.png?size=64 "Alex^#1629")
![Nover#9563](https://cdn.discordapp.com/avatars/248726456551604224/4f22c1d6e37874987470c1af7dc21d10.png?size=64 "Nover#9563")
            

## 5. Disclaimer

This is a program made by the community, and therefore not affiliated to Blade in any way.
 
Moreover, there is no confidence that it will work forever, nor that it fits the same security standard Shadow uses. Keep in mind that we cannot guarantee anything as we aren't professionnals.
