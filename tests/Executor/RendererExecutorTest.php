<?php declare(strict_types=1);

namespace Tests\ShadowConnect\Executor;

use Tests\ShadowConnect\Resources\Executor\RendererExecutorProxy;
use Tests\ShadowConnect\Resources\TestCase;

/**
 * Class RendererExecutorTest
 *
 * @author    Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright 2014 - 2020 RichCongress (https://www.richcongress.com)
 */
class RendererExecutorTest extends TestCase
{
    /** @var RendererExecutorProxy */
    protected $executor;

    /**
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->executor = new RendererExecutorProxy();
    }

    public function testStart(): void
    {
        $execMock = $this->getFunctionMock('ShadowConnect\Executor', 'exec');
        $execMock->expects(self::once())
            ->willReturnCallback(
                static function ($command) {
                    self::assertEquals('cd /path/to/shadow/renderer && ./Shadow FLAGS > /dev/null 2>&1', $command);
                }
            );

        $this->executor->start();
    }
}
