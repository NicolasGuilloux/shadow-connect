<?php declare(strict_types=1);

namespace Tests\ShadowConnect\Resources;

use phpmock\phpunit\PHPMock;
use ShadowConnect\Kernel;

/**
 * Class TestCase
 *
 * @package    Tests\ShadowConnect\Resources
 * @author     Nicolas Guilloux <nguilloux@richcongress.com>
 * @copyright  2014 - 2020 RichCongress (https://www.richcongress.com)
 */
abstract class TestCase extends \PHPUnit\Framework\TestCase
{
    use PHPMock;

    /** @var Kernel */
    private $kernel;

    /**
     * @return void
     */
    public function setUp(): void
    {
        $this->kernel = new Kernel();
    }
}
