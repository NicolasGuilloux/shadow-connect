<?php
declare(strict_types=1);

namespace Tests\ShadowConnect\Resources\Executor;

use ShadowConnect\Executor\RendererExecutor;
use ShadowConnect\Generator\RendererFlagsGenerator;

class RendererExecutorProxy extends RendererExecutor
{
    public function __construct()
    {
        parent::__construct();

        /** @var RendererFlagsGenerator $flagsGenerator */
        $flagsGenerator = static function () {
            return 'FLAGS';
        };

        $this->flagsGenerator = $flagsGenerator;
    }
}
