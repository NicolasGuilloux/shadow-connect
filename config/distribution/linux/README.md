# Shadow Connect on Linux

## Dependencies

Make sure you installed PHP and its curl extension.

If you want to store the password in your keyring, make sure that `secret-tool` is installed aswell to use GNOME Keyring. KDE Keyring is not yet supported.

## Installation

Simply execute the following script that will install the application in your `~/.local/share` folder.

```bash
curl https://gitlab.com/NicolasGuilloux/shadow-connect/-/raw/master/config/distribution/linux/install.sh | bash
```

## Usage

Simply type `shadow-connect` in a terminal to see what you can do.

- `shadow-connect config-wizard` An installation wizard to help you configure the Shadow Connect.
- `shadow-connect start_vm` Start your Shadow VM.
- `shadow-connect start_vm` Stop your Shadow VM.
- `shadow-connect start_renderer` Start your Shadow stream from a started VM.
- `shadow-connect start` Start your Shadow VM and then the stream.
