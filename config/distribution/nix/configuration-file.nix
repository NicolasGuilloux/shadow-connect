{ writeText,

shadowPackage, email, bitrate, hevc, tcpInput, pcmAudio, fullscreen, quickmenu, maxFramerate, autoShutdown,
uuid, debugChannel }:

let
  booleanValue = value: if value then "true" else "false";
  intOrNull = value: if value == null then "null" else (toString value);
  stringOrNull = value: if value == null then "null" else "'" + value + "'";
in
writeText "config.local.php" ''
<?php declare(strict_types=1);

return [
    'BRANCH'                => '${shadowPackage.channel}',
    'USERNAME'              => '${email}',

    # Platform
    'RENDERER_EXEC_PATH'    => null,
    'RENDERER_EXEC_BIN'     => '${shadowPackage}/bin/shadow-${shadowPackage.channel}-renderer',

    # Configuration
    'BITRATE'               => ${toString bitrate},
    'HEVC'                  => ${booleanValue hevc},
    'TCP'                   => ${booleanValue tcpInput},
    'AUDIO_PCM'             => ${booleanValue pcmAudio},
    'FULLSCREEN'            => ${booleanValue fullscreen},
    'QUICKMENU'             => ${booleanValue quickmenu},
    'MAX_FRAMERATE'         => ${intOrNull maxFramerate },
    'AUTO_SHUTDOWN'         => ${booleanValue autoShutdown},

    # Miscalleneous
    'LOG_LEVEL'             => '${debugChannel}',
];
''

