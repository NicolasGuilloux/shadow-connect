{ config, lib, pkgs, ... }:

with lib;

{
  options.programs.shadow-connect = {
    enable = mkEnableOption "Enable Shadow Connect, a CLI to interact with your Shadow.";

    package = mkOption {
      type = types.package;
      description = "You Shadow package that will be used to start the application.";
    };

    email = mkOption {
      type = types.str;
      description = "Email of your Shadow account.";
    };

    bitrate = mkOption {
      type = types.int;
      default = 20;
      description = "Path to the file that contains your credentials.";
    };

    hevc = mkOption {
      type = types.bool;
      default = false;
      description = "Enable HEVC.";
    };

    tcpInput = mkOption {
      type = types.bool;
      default = false;
      description = "Use TCP protol instead of UDP for the inputs.";
    };

    pcmAudio = mkOption {
      type = types.bool;
      default = false;
      description = "Enable high quality audio.";
    };

    fullscreen = mkOption {
      type = types.bool;
      default = false;
      description = "Enable Fullscreen when the stream starts.";
    };

    quickmenu = mkOption {
      type = types.bool;
      default = true;
      description = "Enable the Quickmenu.";
    };

    maxFramerate = mkOption {
      type = types.nullOr types.int;
      default = null;
      description = "Limit the framerate.";
    };

    autoShutdown = mkOption {
      type = types.bool;
      default = false;
      description = "Shutdown automatically your Shadow when the stream ends.";
    };

    uuid = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "Force UUID. This can be a security issue, be careful!";
    };

    debugChannel = mkOption {
      type = types.enum [ "error" "warn" "info" "debug" ];
      default = "warn";
      description = "Logger level to print in the console.";
    };
  };
}
