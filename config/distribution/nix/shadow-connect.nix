{ stdenv, lib, makeWrapper, php, callPackage, gnome3,

# Input config
shadowPackage, email, bitrate, hevc, tcpInput, pcmAudio, fullscreen, quickmenu, maxFramerate, autoShutdown,
uuid, debugChannel }:

let
  configFile = (callPackage ./configuration-file.nix {
    shadowPackage = shadowPackage;
    email = email;
    bitrate = bitrate;
    hevc = hevc;
    tcpInput = tcpInput;
    pcmAudio = pcmAudio;
    fullscreen = fullscreen;
    quickmenu = quickmenu;
    maxFramerate = maxFramerate;
    autoShutdown = autoShutdown;
    uuid = uuid;
    debugChannel = debugChannel;
  });
in
stdenv.mkDerivation rec {
  pname = "shadow-connect";
  version = "0.1.0";
  src = builtins.path { path = ../../../.; };

  nativeBuildInputs = [ makeWrapper ];
  runtimeDependencies = [ gnome3.libsecret ];

  installPhase = ''
    # Create directory
    mkdir -p $out/{bin,share/${pname}}
    cp -r * $out/share/${pname}/

    # Create wrapper
    makeWrapper "${php}/bin/php" "$out/bin/shadow-connect" \
      --add-flags "$out/share/${pname}/bin/shadow-connect" \
      --prefix LD_LIBRARY_PATH : ${lib.strings.makeLibraryPath runtimeDependencies}

    # Generate config
    ln -sf "${configFile}" "$out/share/${pname}/config/config.local.php"
  '';

  meta = with stdenv.lib; {
    description = "Shadow connect derivation";
    homepage = "https://gitlab.com/NicolasGuilloux/shadow-connect";
    license = licenses.gpl3;
    platforms = platforms.all;
  };
}
