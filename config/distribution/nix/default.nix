{ config, lib, pkgs, ... }:

let
  cfg = config.programs.shadow-connect;
in
{
  imports = [ ./config.nix ];

  # Install the package
  environment.systemPackages = lib.mkIf cfg.enable [
    (pkgs.callPackage ./shadow-connect.nix {
      shadowPackage = cfg.package;
      email = cfg.email;
      bitrate = cfg.bitrate;
      hevc = cfg.hevc;
      tcpInput = cfg.tcpInput;
      pcmAudio = cfg.pcmAudio;
      fullscreen = cfg.fullscreen;
      quickmenu = cfg.quickmenu;
      maxFramerate = cfg.maxFramerate;
      autoShutdown = cfg.autoShutdown;
      uuid = cfg.uuid;
      debugChannel = cfg.debugChannel;
    })
  ];
}
