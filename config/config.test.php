<?php declare(strict_types=1);

return [
    'USERNAME'  => 'test@shadow.tech',
    'PASSWORD'  => 'ThisIsATest',

    # Miscalleneous
    'LOG_LEVEL' => 'error',
];
