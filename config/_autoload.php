<?php declare(strict_types=1);

if (is_dir(__DIR__ . '/../vendor')) {
    require_once __DIR__ . '/../vendor/autoload.php';
    return;
}

spl_autoload_register(
    static function (string $className): void
    {
        $className = str_replace('\\', '/', $className);
        $path = str_replace('ShadowConnect', 'src', $className) . '.php';

        include_once __DIR__ . '/../' . $path;
    }
);
