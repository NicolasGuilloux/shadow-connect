<?php declare(strict_types=1);

return [
    'BRANCH'                => 'prod',

    # Platform
    'RENDERER_EXEC_PATH'    => '/path/to/shadow/renderer',
    'RENDERER_EXEC_BIN'     => 'Shadow',

    # Configuration
    'BITRATE'               => 20000000,
    'HEVC'                  => false,
    'USB'                   => false,
    'TCP'                   => false,
    'AUDIO_PCM'             => false,
    'FULLSCREEN'            => false,
    'QUICKMENU'             => true,
    'DISPLAY_SAFE'          => false,
    'EXPERIMENTAL_FEATURES' => false,
    'MAX_FRAMERATE'         => null,
    'AUTO_SHUTDOWN'         => false,

    # Miscalleneous
    'STOP_VM'               => false,
    'VM_TRIGGER_UPDATE'     => true,
    'START_VM_IDLER'        => false,
    'LOG_LEVEL'             => 'info',
];
