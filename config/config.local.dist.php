<?php declare(strict_types=1);

return [
    /**
     * The following option is required, you need to set it in order to make the application work.
     * This must be the path to the renderer (NOT THE LAUNCHER)
     */
    'RENDERER_EXEC_PATH'    => '/path/to/shadow/resources/app.asar.unpacked/release/native/',

    'USERNAME'              => 'my@shadowemail.com', // Your email used for your Shadow account
    'BRANCH'                => 'prod',               // prod, preprod, testing
    'BITRATE'               => 20,                   // From 5 to 70 Mbits/s
    'HEVC'                  => false,                // HEVC (H265) decoding
    'USB'                   => false,                // Enable USB
    'TCP'                   => false,                // Enable TCP transport protocol for the inputs, rather than UDP
    'AUDIO_PCM'             => false,                // Enable high quality audio
    'FULLSCREEN'            => false,                // Enable fullscreen when the stream starts
    'QUICKMENU'             => true,                 // Enable the Quickmenu
    'MAX_FRAMERATE'         => null,                 // Lock the maximum framerate
    'AUTO_SHUTDOWN'         => false,                // Shutdown your VM when the stream ends
];
